variable "aws_account_id" {
  type = "string"
}

variable "region" {
  type        = "string"
  description = "Aws account region"
}

variable "repo_name" {
  type        = "string"
  description = "Repo name"
  default     = "example"
}

variable "repo_description" {
  type = "string"
  default = "Default descrtiption"
}
