resource "aws_codecommit_repository" "this_repo" {
  repository_name = "${var.repo_name}"
  description     = "${var.repo_description}"
}
output "arn" {
  value = "${aws_codecommit_repository.this_repo.arn}"
}


data "template_file" "CodeCommitPower" {
  template = "${file("${path.module}/templates/CodeCommitPower.tpl")}"

  vars {
    repo_name   = "${var.repo_name}"
    aws_account_id = "${var.aws_account_id}"
    region         = "${var.region}"
  }
}
resource "aws_iam_policy" "CodeCommitPower" {
  name        = "${var.repo_name}-CodeCommitPower"
  path        = "/"
  description = "Full access to ${var.repo_name} git repo"

  policy = "${data.template_file.CodeCommitPower.rendered}"
}
output "policy_codecommitpower_arn" {
  value = "${aws_iam_policy.CodeCommitPower.arn}"
}


data "template_file" "CodeCommitRO" {
  template = "${file("${path.module}/templates/CodeCommitRO.tpl")}"

  vars {
    repo_name   = "${var.repo_name}"
    aws_account_id = "${var.aws_account_id}"
    region         = "${var.region}"
  }
}

resource "aws_iam_policy" "CodeCommitRO" {
  name        = "${var.repo_name}-CodeCommitRO"
  path        = "/"
  description = "Full access to ${var.repo_name} git repo"

  policy = "${data.template_file.CodeCommitRO.rendered}"
}
output "policy_codecommitro_arn" {
  value = "${aws_iam_policy.CodeCommitRO.arn}"
}


